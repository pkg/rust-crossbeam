Source: rust-crossbeam
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-cfg-if-1+default-dev <!nocheck>,
 librust-crossbeam-channel-0.5+std-dev <!nocheck>,
 librust-crossbeam-deque-0.8+std-dev <!nocheck>,
 librust-crossbeam-epoch-0.9+alloc-dev (>= 0.9.5-~~) <!nocheck>,
 librust-crossbeam-epoch-0.9+std-dev (>= 0.9.5-~~) <!nocheck>,
 librust-crossbeam-queue-0.3+alloc-dev (>= 0.3.2-~~) <!nocheck>,
 librust-crossbeam-queue-0.3+std-dev (>= 0.3.2-~~) <!nocheck>,
 librust-crossbeam-utils-0.8+std-dev (>= 0.8.5-~~) <!nocheck>,
 librust-crossbeam-utils-0.8-dev (>= 0.8.5-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Paul van Tilburg <paulvt@debian.org>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/crossbeam]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/crossbeam
Homepage: https://github.com/crossbeam-rs/crossbeam
Rules-Requires-Root: no

Package: librust-crossbeam-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-cfg-if-1+default-dev,
 librust-crossbeam-channel-0.5+std-dev,
 librust-crossbeam-channel-0.5-dev,
 librust-crossbeam-deque-0.8+std-dev,
 librust-crossbeam-deque-0.8-dev,
 librust-crossbeam-epoch-0.9+alloc-dev (>= 0.9.5-~~),
 librust-crossbeam-epoch-0.9+std-dev (>= 0.9.5-~~),
 librust-crossbeam-epoch-0.9-dev (>= 0.9.5-~~),
 librust-crossbeam-queue-0.3+alloc-dev (>= 0.3.2-~~),
 librust-crossbeam-queue-0.3+std-dev (>= 0.3.2-~~),
 librust-crossbeam-queue-0.3-dev (>= 0.3.2-~~),
 librust-crossbeam-utils-0.8+std-dev (>= 0.8.5-~~),
 librust-crossbeam-utils-0.8-dev (>= 0.8.5-~~)
Provides:
 librust-crossbeam+alloc-dev (= ${binary:Version}),
 librust-crossbeam+crossbeam-channel-dev (= ${binary:Version}),
 librust-crossbeam+crossbeam-deque-dev (= ${binary:Version}),
 librust-crossbeam+crossbeam-epoch-dev (= ${binary:Version}),
 librust-crossbeam+crossbeam-queue-dev (= ${binary:Version}),
 librust-crossbeam+default-dev (= ${binary:Version}),
 librust-crossbeam+std-dev (= ${binary:Version}),
 librust-crossbeam-0-dev (= ${binary:Version}),
 librust-crossbeam-0+alloc-dev (= ${binary:Version}),
 librust-crossbeam-0+crossbeam-channel-dev (= ${binary:Version}),
 librust-crossbeam-0+crossbeam-deque-dev (= ${binary:Version}),
 librust-crossbeam-0+crossbeam-epoch-dev (= ${binary:Version}),
 librust-crossbeam-0+crossbeam-queue-dev (= ${binary:Version}),
 librust-crossbeam-0+default-dev (= ${binary:Version}),
 librust-crossbeam-0+std-dev (= ${binary:Version}),
 librust-crossbeam-0.8-dev (= ${binary:Version}),
 librust-crossbeam-0.8+alloc-dev (= ${binary:Version}),
 librust-crossbeam-0.8+crossbeam-channel-dev (= ${binary:Version}),
 librust-crossbeam-0.8+crossbeam-deque-dev (= ${binary:Version}),
 librust-crossbeam-0.8+crossbeam-epoch-dev (= ${binary:Version}),
 librust-crossbeam-0.8+crossbeam-queue-dev (= ${binary:Version}),
 librust-crossbeam-0.8+default-dev (= ${binary:Version}),
 librust-crossbeam-0.8+std-dev (= ${binary:Version}),
 librust-crossbeam-0.8.1-dev (= ${binary:Version}),
 librust-crossbeam-0.8.1+alloc-dev (= ${binary:Version}),
 librust-crossbeam-0.8.1+crossbeam-channel-dev (= ${binary:Version}),
 librust-crossbeam-0.8.1+crossbeam-deque-dev (= ${binary:Version}),
 librust-crossbeam-0.8.1+crossbeam-epoch-dev (= ${binary:Version}),
 librust-crossbeam-0.8.1+crossbeam-queue-dev (= ${binary:Version}),
 librust-crossbeam-0.8.1+default-dev (= ${binary:Version}),
 librust-crossbeam-0.8.1+std-dev (= ${binary:Version})
Description: Support for concurrent and parallel programming - Rust source code
 This package contains the source for the Rust crossbeam crate, packaged by
 debcargo for use with cargo and dh-cargo.
